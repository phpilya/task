<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('payment')->as('payment.')->group(function () {
    Route::post('/first', [\App\Http\Controllers\PaymentController::class, 'first'])->name('first');
    Route::post('/second', [\App\Http\Controllers\PaymentController::class, 'second'])->name('second');
});